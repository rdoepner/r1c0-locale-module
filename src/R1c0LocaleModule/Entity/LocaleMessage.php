<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */
namespace R1c0LocaleModule\Entity;

use R1c0LocaleModule\Entity\Locale;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="r1c0_locale_message")
 */
class LocaleMessage
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="text_domain", type="string", length=255)
     */
    protected $textDomain;

    /**
     * @ORM\Column(name="message_key", type="text")
     */
    protected $messageKey;

    /**
     * @ORM\Column(name="message_translation", type="text")
     */
    protected $messageTranslation;

    /**
     * @ORM\Column(name="plural_index", type="smallint")
     */
    protected $pluralIndex;

    /**
     * @ORM\ManyToOne(targetEntity="Locale", inversedBy="messages")
     */
    protected $locale;

    public function getId()
    {
        return $this->id;
    }

    public function setTextDomain($textDomain)
    {
        $this->textDomain = $textDomain;
        
        return $this;
    }

    public function getTextDomain()
    {
        return $this->textDomain;
    }

    public function setMessageKey($messageKey)
    {
        $this->messageKey = $messageKey;
        
        return $this;
    }

    public function getMessageKey()
    {
        return $this->messageKey;
    }

    public function setMessageTranslation($messageTranslation)
    {
        $this->messageTranslation = $messageTranslation;
        
        return $this;
    }

    public function getMessageTranslation()
    {
        return $this->messageTranslation;
    }

    public function setPluralIndex($pluralIndex)
    {
        $this->pluralIndex = $pluralIndex;
        
        return $this;
    }

    public function getPluralIndex()
    {
        return $this->pluralIndex;
    }

    public function setLocale(Locale $locale)
    {
        $this->locale = $locale;
        
        return $this;
    }

    public function getLocale()
    {
        return $this->locale;
    }
}
