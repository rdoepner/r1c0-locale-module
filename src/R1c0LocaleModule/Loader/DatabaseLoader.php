<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */
namespace R1c0LocaleModule\Loader;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\I18n\Translator\Loader\RemoteLoaderInterface;
use Zend\I18n\Translator\Plural\Rule;
use Zend\I18n\Translator\TextDomain;

class DatabaseLoader implements RemoteLoaderInterface, ServiceLocatorAwareInterface
{

    protected $serviceLocator;

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        
        return $this;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function load($locale, $textDomain)
    {
        $loaderPluginManager = $this->getServiceLocator();
        $serviceLocator = $loaderPluginManager->getServiceLocator();
        $entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $localeRepository = $entityManager->getRepository('R1c0LocaleModule\Entity\Locale');
        $textDomainObject = new TextDomain();
        
        if ($localeEntity = $localeRepository->find($locale)) {
            $textDomainObject->setPluralRule(Rule::fromString($localeEntity->getPluralForms()));
            
            foreach ($localeEntity->getMessages() as $messageEntity) {
                if ($messageEntity->getTextDomain() == $textDomain) {
                    $messageKey = $messageEntity->getMessageKey();
                    $messageTranslation = $messageEntity->getMessageTranslation();
                    $pluralIndex = $messageEntity->getPluralIndex();
                    
                    if (isset($textDomainObject[$messageKey])) {
                        if (! is_array($textDomainObject[$messageKey])) {
                            $textDomainObject[$messageKey] = array(
                                $pluralIndex => $textDomainObject[$messageKey]
                            );
                        }
                        
                        $textDomainObject[$messageKey][$pluralIndex] = $messageTranslation;
                    } else {
                        $textDomainObject[$messageKey] = $messageTranslation;
                    }
                }
            }
        }
        
        return $textDomainObject;
    }
}
