<?php

/**
 * Copyright (c) 2014-2015, Rico Döpner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the names of the copyright holders nor the names of the
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author      Rico Döpner <kontakt@rico-doepner.de>
 * @copyright   2014-2015, Rico Döpner
 * @license     http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link        http://rico-doepner.de/
 */
namespace R1c0LocaleModule;

use R1c0LocaleModule\Options\ModuleOptions;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventInterface;
use Zend\Loader\StandardAutoloader;
use Zend\Loader\AutoloaderFactory;
use Zend\I18n\Translator\LoaderPluginManager;
use Zend\I18n\Translator\Translator;
use Zend\Validator\AbstractValidator;
use Locale;

class Module implements BootstrapListenerInterface, AutoloaderProviderInterface, ConfigProviderInterface, ServiceProviderInterface
{

    public function onBootstrap(EventInterface $mvcEvent)
    {
        $serviceManager = $mvcEvent->getApplication()->getServiceManager();
        $moduleOptions = $serviceManager->get('R1c0LocaleModule\Options\ModuleOptions');
        $mvcTranslator = $serviceManager->get('MvcTranslator');
        Locale::setDefault($moduleOptions->getDefault());
        
        if ($mvcTranslator->getTranslator() instanceof Translator) {
            $loaderPluginManager = new LoaderPluginManager();
            $loaderPluginManager->setServiceLocator($serviceManager);
            $mvcTranslator->setPluginManager($loaderPluginManager);
        }
        
        AbstractValidator::setDefaultTranslator($mvcTranslator);
    }

    public function getAutoloaderConfig()
    {
        return array(
            AutoloaderFactory::STANDARD_AUTOLOADER => array(
                StandardAutoloader::LOAD_NS => array(
                    __NAMESPACE__ => __DIR__
                )
            )
        );
    }

    public function getConfig()
    {
        return require_once __DIR__ . "/../../config/module.config.php";
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'R1c0LocaleModule\Options\ModuleOptions' => function (ServiceLocatorInterface $serviceLocator) {
                    
                    $moduleOptions = new ModuleOptions();
                    $mergedConfig = $serviceLocator->get('Config');
                    
                    if (isset($mergedConfig['r1c0']['locale'])) {
                        $moduleOptions->setFromArray($mergedConfig['r1c0']['locale']);
                    }
                    
                    return $moduleOptions;
                }
            )
        );
    }
}
