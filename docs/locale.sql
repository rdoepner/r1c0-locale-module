-- -----------------------------------------------------
-- Table `r1c0_locale`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `r1c0_locale`;

CREATE TABLE IF NOT EXISTS `r1c0_locale` (
  `id` CHAR(5) NOT NULL,
  `plural_forms` VARCHAR(100) NOT NULL DEFAULT 'nplurals=2; plural=(n==1 ? 0 : 1)',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

-- -----------------------------------------------------
-- Table data `r1c0_locale`
-- -----------------------------------------------------
INSERT INTO `r1c0_locale` (`id`, `plural_forms`) VALUES
('de_DE', 'nplurals=2; plural=(n==1 ? 0 : 1)');

-- -----------------------------------------------------
-- Table `r1c0_locale_message`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `r1c0_locale_message`;

CREATE TABLE IF NOT EXISTS `r1c0_locale_message` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `locale_id` CHAR(5) NOT NULL,
  `text_domain` VARCHAR(255) NOT NULL DEFAULT 'default',
  `message_key` TEXT NOT NULL,
  `message_translation` TEXT NOT NULL,
  `plural_index` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
    FOREIGN KEY (`locale_id`)
    REFERENCES `r1c0_locale` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

-- -----------------------------------------------------
-- Table data `r1c0_locale_message`
-- -----------------------------------------------------
INSERT INTO `r1c0_locale_message` (`id`, `locale_id`, `text_domain`, `message_key`, `message_translation`, `plural_index`) VALUES
(1, 'de_DE', 'default', 'Hello World', 'Hallo Welt', 1);
